Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'couriers#index', as: 'home'

  resources :couriers do 
  	resources :packages
  end


  get 'about' => 'deliveries#about', as:'about'
end
