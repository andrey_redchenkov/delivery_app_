# README

## Controller layer
The Controller layer is responsible for handling incoming HTTP requests and providing a suitable response. Usually, this means returning HTML, but Rails controllers can also generate XML, JSON, PDFs, mobile-specific views, and more. Controllers load and manipulate models, and render view templates in order to generate the appropriate HTTP response. In Rails, incoming requests are routed by Action Dispatch to an appropriate controller, and controller classes are derived from ActionController::Base. Action Dispatch and Action Controller are bundled together in Action Pack.

## View layer
The View layer is composed of "templates" that are responsible for providing appropriate representations of your application's resources. Templates can come in a variety of formats, but most view templates are HTML with embedded Ruby code (ERB files). Views are typically rendered to generate a controller response or to generate the body of an email. In Rails, View generation is handled by Action View.
Some test test goes here!

## Frameworks and libraries
Active Record, Active Model, Action Pack, and Action View can each be used independently outside Rails. In addition to that, Rails also comes with Action Mailer, a library to generate and send emails; Action Mailbox, a library to receive emails within a Rails application; Active Job, a framework for declaring jobs and making them run on a variety of queuing backends; Action Cable, a framework to integrate WebSockets with a Rails application; Active Storage, a library to attach cloud and local files to Rails applications; Action Text, a library to handle rich text content; and Active Support, a collection of utility classes and standard library extensions that are useful for Rails, and may also be used independently outside Rails.
* Ruby version:

    _ruby 2.6.6p146 (2020-03-31 revision 67876)_

* Database initialization:

    bin/rails `db:migrate RAILS_ENV=development`

* How to run the app:

    Change directory to delivery_app and start the web server:
    
    `$ cd delivery_app`
    
    `$ bin/rails server`
    
    Go to `http://localhost:3000`

* How to run the test suite
    Some test test goes here!


    The description goes here:
    

