class Courier < ApplicationRecord
	has_many :packages
	validates :name, presence: true, length: {minimum: 3}
end